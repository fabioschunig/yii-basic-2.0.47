<?php

/** @var yii\web\View $this */
/** @var string $message */

use yii\helpers\Html;

$this->title = 'Say file';
?>

<?= Html::encode($message) ?>
